package com.tsc.ProductWarehouse.DAO.mappers;

import com.tsc.ProductWarehouse.DAO.interfaces.ListRowMapper;
import com.tsc.ProductWarehouse.domain.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Маппер списка пользователей
 *
 * @author Дмитрий Лаврухин
 */
public class UserListMapper implements ListRowMapper<User> {
    private final UserMapper mapper = new UserMapper();

    @Override
    public List<User> mapRow(final ResultSet resultSet) throws SQLException {
        final List<User> userList = new ArrayList<>();
        while (resultSet.next()) {
            final User user = mapper.mapRow(resultSet);
            userList.add(user);
        }
        return userList;
    }
}
