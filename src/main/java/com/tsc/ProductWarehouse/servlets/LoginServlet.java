package com.tsc.ProductWarehouse.servlets;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.ProductWarehouse.DAO.exceptions.UserDAOException;
import com.tsc.ProductWarehouse.domain.entity.User;
import com.tsc.ProductWarehouse.service.UserService;
import com.tsc.ProductWarehouse.service.exception.UserServiceException;
import com.tsc.ProductWarehouse.util.JWTUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Сервлет для получения JWT токена пользователя.
 *
 * @author Дмитрий Лаврухин
 */
public class LoginServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final UserService userService = new UserService();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        try {
            final User loginUser = objectMapper.readValue(request.getReader(), User.class);
            String key = userService.userLoginJwt(loginUser);
            response.addHeader(JWTUtil.TOKEN_HEADER, key);
            logger.info("Выполнена аутентификация пользователя с email = " + loginUser.getEmail());
        } catch (UserServiceException | UserDAOException e) {
            writeToResponseErrorCodeAndMessage(response, 401, e.getMessage());
        } catch (JsonProcessingException e) {
            writeToResponseErrorCodeAndMessage(response, 401, "Ошибка во входных данных.");
        }

    }

    private void writeToResponseErrorCodeAndMessage(final HttpServletResponse response,
                                                    final int errorCode,
                                                    final String errorMessage) throws IOException {
        response.setStatus(errorCode);
        response.getWriter().println("{" +
                "\"error message\": " +
                "\"" + errorMessage + "\"" +
                "}");
    }
}
