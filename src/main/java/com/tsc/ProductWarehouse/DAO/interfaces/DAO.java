package com.tsc.ProductWarehouse.DAO.interfaces;

import java.util.List;

/**
 * Интерфейс со стандартными DAO методами
 *
 * @param <T> тип объекта
 * @author Дмитрий Лаврухин
 */
public interface DAO<T> {

    List<T> getAll(int pageNumber, int pageSize);

    T getById(int id);

    int insertRecord(T newRecord);

    void updateRecord(int id, T newRecord);

    void deleteRecord(int id);
}
