package com.tsc.ProductWarehouse.filter;

import com.tsc.ProductWarehouse.util.JWTUtil;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class AccessFilterToModifyUserDataTest {

    private final String userIdInJwt = "12";

    private final String userAdminToken = "Bearer eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJzdWIiOiIxMiIsImVtYWlsIjoi" +
            "MTIzQG1haWwucnUiLCJyb2wiOiJBRE1JTiJ9.1Db3KHqPOEyBkZSfE6SEvwUhuZ452qz1fV_rMoVrGPu1zV0ZbY7FwcKgzfBQhHm_F6nf" +
            "yTS-DiFOtyrBOVi9Ew";

    private final String userClientJwt = "Bearer eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJzdWIiOiIxMiIsImVtYWlsIjoiM" +
            "TIzQG1haWwucnUiLCJyb2wiOiJDTElFTlQifQ.UwVMMbMTUa4BoQsx9WwY0pw_iTsOfmzgb9aJE_Rz1ub5Es-fHwrR-8awy3qcm3NR_ND" +
            "uCeL-FBYWpEypwu2B3w";

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private FilterChain filterChain;

    @BeforeEach
    void setUp() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        filterChain = mock(FilterChain.class);
    }

    @Test
    void doFilter_shouldInvokeDoFilter_WhenUserIsAdmin() throws ServletException, IOException {
        when(request.getHeader(JWTUtil.TOKEN_HEADER)).thenReturn(userAdminToken);
        new AccessFilterToModifyUserData().doFilter(request, response, filterChain);
        verify(filterChain).doFilter(request, response);
    }

    @Test
    void doFilter_shouldInvokeDoFilter_WhenJwtIdNotEqualsRequestParameterId() throws ServletException, IOException {
        when(request.getMethod()).thenReturn("GET");
        when(request.getHeader(JWTUtil.TOKEN_HEADER)).thenReturn(userClientJwt);
        when(request.getParameter("id")).thenReturn(userIdInJwt);
        new AccessFilterToModifyUserData().doFilter(request, response, filterChain);
        verify(filterChain).doFilter(request, response);
    }

    @Test
    void doFilter_shouldReturnErrorCodeAndErrorMessage_WhenJwtIdNotEqualsRequestParameterId() throws ServletException, IOException {
        final StringWriter stringWriter = new StringWriter();
        final PrintWriter writer = new PrintWriter(stringWriter);

        when(response.getWriter()).thenReturn(writer);
        when(request.getMethod()).thenReturn("GET");
        when(request.getHeader(JWTUtil.TOKEN_HEADER)).thenReturn(userClientJwt);
        when(request.getParameter("id")).thenReturn("15");

        new AccessFilterToModifyUserData().doFilter(request, response, filterChain);
        verify(response).setStatus(403);
        writer.flush();
        assertThat(stringWriter.toString()).contains("Отказано в доступе");
    }
}