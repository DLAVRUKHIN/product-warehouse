package com.tsc.ProductWarehouse.filter;

import com.tsc.ProductWarehouse.domain.enums.UserRole;
import com.tsc.ProductWarehouse.util.JWTUtil;
import com.tsc.ProductWarehouse.util.exception.JWTException;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Optional;

/**
 * Фильтр для ограничения доступа к изменению данных пользователей.
 * Пользователь может изменять только свои данные. Администратор может изменять любые данные.
 *
 * @author Дмитрий Лаврухин
 */
@WebFilter("/app/user")
public class AccessFilterToModifyUserData implements Filter {
    private final Logger logger = LogManager.getLogger();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        try {
            final String jwtToken = request.getHeader(JWTUtil.TOKEN_HEADER);
            final UserRole userRole = JWTUtil.getRoleFormToken(jwtToken);
            final String method = request.getMethod();

            if (userRole.equals(UserRole.ADMIN)) {
                filterChain.doFilter(request, response);
            } else if ((method.equals("GET") || method.equals("PUT"))
                    && requestOwnerIdEqualsUserId(request, jwtToken)) {
                filterChain.doFilter(request, response);
            } else {
                writeToResponseErrorCodeAndMessage(response, 403, "Отказано в доступе");
            }
        } catch (JWTException e) {
            logger.error("Выполнен запрос с невалидным JWT токеном");
            writeToResponseErrorCodeAndMessage(response, 401, "Необходимо предоставить " +
                    "валидный JWT токен");
        }
    }

    private boolean requestOwnerIdEqualsUserId(final HttpServletRequest request, final String jwt) {
        final Optional<String> userId = Optional.ofNullable(request.getParameter("id"));
        final String ownerId = JWTUtil.getUserIdFormToken(jwt);
        return userId.map(s -> s.equals(ownerId)).orElse(false);
    }

    private void writeToResponseErrorCodeAndMessage(final HttpServletResponse response,
                                                    final int errorCode,
                                                    final String errorMessage) throws IOException {
        response.setStatus(errorCode);
        response.getWriter().println("{" +
                "\"error message\": " +
                "\"" + errorMessage + "\"" +
                "}");
    }
}
