package com.tsc.ProductWarehouse.DAO.mappers;

import com.tsc.ProductWarehouse.DAO.interfaces.RowMapper;
import com.tsc.ProductWarehouse.domain.entity.User;
import com.tsc.ProductWarehouse.domain.enums.UserRole;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Маппер пользователя
 *
 * @author Дмитрий Лаврухин
 */
public class UserMapper implements RowMapper<User> {

    @Override
    public User mapRow(final ResultSet resultSet) throws SQLException {
        final int id = resultSet.getInt("id");
        final UserRole userRole = UserRole.valueOf(resultSet.getString("role"));
        final String name = resultSet.getString("name");
        final String surname = resultSet.getString("surname");
        final String email = resultSet.getString("email");
        final String phone = resultSet.getString("phone");
        return new User(id, userRole, name, surname, email, phone);
    }
}
