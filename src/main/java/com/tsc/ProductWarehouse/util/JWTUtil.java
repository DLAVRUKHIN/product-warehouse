package com.tsc.ProductWarehouse.util;

import com.tsc.ProductWarehouse.domain.enums.UserRole;
import com.tsc.ProductWarehouse.util.exception.JWTException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

/**
 * Класс реализующий методы создания JWT токена, а так же получения информации из него.
 *
 * @author Дмитрий Лаврухин
 */
public class JWTUtil {

    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
    private static final String JWT_SECRET = "n2r5u8x/A%D*G-KaPdSgVkYp3s6v9y$B&E(H+MbQeThWmZq4t7w!z%C*F-J@NcRf";

    public static String createJWT(final String userId, final String userEmail, final UserRole role) {
        final byte[] signingKey = JWT_SECRET.getBytes();
        final String jwt = Jwts.builder()
                .signWith(Keys.hmacShaKeyFor(signingKey), SignatureAlgorithm.HS512)
                .setHeaderParam("type", TOKEN_TYPE)
                .setSubject(userId)
                .claim("email", userEmail)
                .claim("rol", role)
                .compact();
        return TOKEN_PREFIX + jwt;
    }

    public static UserRole getRoleFormToken(final String jwt) {
        final byte[] signingKey = JWT_SECRET.getBytes();
        try {
            final Jws<Claims> jws = Jwts.parserBuilder()
                    .setSigningKey(signingKey)
                    .build()
                    .parseClaimsJws(jwt.replace(TOKEN_PREFIX, ""));
            return UserRole.valueOf(jws.getBody().get("rol").toString());
        } catch (Exception e) {
            throw new JWTException("Ошибка при распознавании информации токена", e);
        }
    }

    public static String getUserIdFormToken(final String jwt) {
        final byte[] signingKey = JWT_SECRET.getBytes();
        try {
            final Jws<Claims> jws = Jwts.parserBuilder()
                    .setSigningKey(signingKey)
                    .build()
                    .parseClaimsJws(jwt.replace(TOKEN_PREFIX, ""));
            return jws.getBody().get("sub").toString();
        } catch (Exception e) {
            throw new JWTException("Ошибка при распознавании информации токена", e);
        }
    }
}
