package com.tsc.ProductWarehouse.DAO.interfaces;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Интерфейс преобразования ResultSet к объекту
 *
 * @param <T> тип объекта
 * @author Дмитрий Лаврухин
 */
public interface RowMapper<T> {
    T mapRow(ResultSet resultSet) throws SQLException;
}
