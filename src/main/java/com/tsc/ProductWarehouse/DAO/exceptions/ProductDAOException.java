package com.tsc.ProductWarehouse.DAO.exceptions;

public class ProductDAOException extends DAOException{
    public ProductDAOException() {
        super();
    }

    public ProductDAOException(String message) {
        super(message);
    }

    public ProductDAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProductDAOException(Throwable cause) {
        super(cause);
    }
}
