package com.tsc.ProductWarehouse.servlets;

import com.tsc.ProductWarehouse.DAO.impl.UserDAO;
import com.tsc.ProductWarehouse.domain.entity.User;
import com.tsc.ProductWarehouse.util.JWTUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class SignupServletTest {
    private final String clientJwt = "Bearer eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJzdWIiOiIxMiIsImVtYWls" +
            "IjoiMTIzQG1haWwucnUiLCJyb2wiOiJjbGllbnQifQ._I3u2ClLYrPtd9zyNnM7pxOG23qYqHFDO7dflfnAeVEHbuneKWy4SEw" +
            "UxVLtrPHcDjuRqg0t7JkgVJNaKWyzGg";

    private final String adminJwt = "Bearer eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJzdWIiOiIxMiIsImVtYWlsIjoiM" +
            "TIzQG1haWwucnUiLCJyb2wiOiJBRE1JTiJ9.1Db3KHqPOEyBkZSfE6SEvwUhuZ452qz1fV_rMoVrGPu1zV0ZbY7FwcKgzfBQhHm_F" +
            "6nfyTS-DiFOtyrBOVi9Ew";

    private final String userInfo = "{\n" +
            "    \"name\" : \"firstUser\", \n" +
            "    \"email\" : \"123@mail.ru\",\n" +
            "    \"password\" : \"123456\"\n" +
            "}";

    private final String emptyPasswordUserInfo = "{\n" +
            "    \"name\" : \"firstUser\", \n" +
            "    \"email\" : \"123@mail.ru\",\n" +
            "    \"password\" : \"\"\n" +
            "}";

    @Mock
    private UserDAO dao;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;

    private StringWriter stringWriter;
    private PrintWriter writer;

    @BeforeEach
    void setUp() throws IOException {
        MockitoAnnotations.openMocks(this);
        stringWriter = new StringWriter();
        writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);
    }

    @Disabled
    @Test
    void doPost_mustReturnAdminJWT_whenUserCreate() throws IOException, ServletException {
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(userInfo)));
        when(dao.getUserByEmail("123@mail.ru")).thenReturn(null);
        when(dao.getCountUser()).thenReturn(0);
        when(dao.insertRecord(any())).thenReturn(12);

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new SignupServlet().doPost(request, response);
            verify(response).addHeader(JWTUtil.TOKEN_HEADER, adminJwt);
            verify(response).setStatus(200);
            assertThat(stringWriter.toString()).isEqualTo("{\"id\" : \"" + 12 + "\"}");
        }
    }

    @Disabled
    @Test
    void doPost_mustReturnErrorMessage_whenUserExists() throws IOException, ServletException {
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(userInfo)));
        when(dao.getUserByEmail("123@mail.ru")).thenReturn(new User());
        when(dao.insertRecord(any())).thenReturn(12);

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new SignupServlet().doPost(request, response);
            verify(response).setStatus(409);
            assertThat(stringWriter.toString()).contains("Пользователь с таким email уже зарегистрирован");
        }
    }

    @Disabled
    @Test
    void doPost_mustReturnErrorMessage_whenPasswordIsEmpty() throws IOException, ServletException {
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(emptyPasswordUserInfo)));
        when(dao.getUserByEmail("123@mail.ru")).thenReturn(null);
        when(dao.insertRecord(any())).thenReturn(12);

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new SignupServlet().doPost(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Не указан логин/пароль.");
        }
    }
}