package com.tsc.ProductWarehouse.DAO.mappers;

import com.tsc.ProductWarehouse.DAO.interfaces.ListRowMapper;
import com.tsc.ProductWarehouse.domain.entity.Product;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Маппер для списка продуктов
 *
 * @author Дмитрий Лаврухин
 */
public class ProductListMapper implements ListRowMapper<Product> {
    private final ProductMapper mapper = new ProductMapper();

    @Override
    public List<Product> mapRow(final ResultSet resultSet) throws SQLException {
        final List<Product> productList = new ArrayList<>();
        while (resultSet.next()) {
            final Product product = mapper.mapRow(resultSet);
            productList.add(product);
        }
        return productList;
    }
}
