package com.tsc.ProductWarehouse.DAO.impl;

import com.tsc.ProductWarehouse.DAO.exceptions.UserDAOException;
import com.tsc.ProductWarehouse.DAO.mappers.UserListMapper;
import com.tsc.ProductWarehouse.DAO.mappers.UserMapper;
import com.tsc.ProductWarehouse.DAO.queryStringBuilder.UserQueryStringBuilder;
import com.tsc.ProductWarehouse.domain.entity.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static java.util.Objects.isNull;

/**
 * DAO объект с методами для управления пользователями
 *
 * @author Дмитрий Лаврухин
 */
public class UserDAO extends DefaultDao<User> {
    private static UserDAO instance;

    private UserDAO() {
        super(new UserMapper(), new UserListMapper());
    }

    public static UserDAO getInstance() {
        if (isNull(instance)) {
            instance = new UserDAO();
        }
        return instance;
    }

    @Override
    public List<User> getAll(final int pageNumber, final int pageSize) {
        final String queryString = UserQueryStringBuilder.builder()
                .getAllUser()
                .pageNumber(pageNumber, pageSize)
                .build();
        return super.getAll(queryString);
    }

    @Override
    public User getById(final int id) {
        final String queryString = UserQueryStringBuilder.builder()
                .getAllUser()
                .where()
                .id()
                .build();
        return super.getById(queryString, id);
    }

    @Override
    public void deleteRecord(final int id) {
        final String queryString = UserQueryStringBuilder.builder()
                .delete()
                .where()
                .id()
                .build();
        super.deleteRecord(queryString, id);
    }

    @Override
    public int insertRecord(final User newRecord) {
        final String queryString = UserQueryStringBuilder.builder()
                .insert()
                .build();
        try (final Connection connection = DriverManager.getConnection(urlConnectDB, dbUser, dbPassword);
             final PreparedStatement statement = connection.prepareStatement(queryString, Statement.RETURN_GENERATED_KEYS)) {
            fillStatement(newRecord, statement);
            statement.setString(6, newRecord.getPassword());
            final int newId = statement.executeUpdate();
            if (newId == 0) {
                throw new SQLException("Не удалось добавить запись в базу данных.");
            }
            return newId;
        } catch (SQLException e) {
            throw new UserDAOException(e);
        }
    }

    @Override
    public void updateRecord(final int id, final User newRecord) {
        final String queryString = UserQueryStringBuilder.builder()
                .update()
                .where()
                .id()
                .build();
        try (final Connection connection = DriverManager.getConnection(urlConnectDB, dbUser, dbPassword);
             final PreparedStatement statement = connection.prepareStatement(queryString)) {
            fillStatement(newRecord, statement);
            statement.setInt(6, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new UserDAOException(e);
        }
    }

    private void fillStatement(User newRecord, PreparedStatement statement) throws SQLException {
        statement.setString(1, newRecord.getUserRole().name());
        statement.setString(2, newRecord.getName());
        statement.setString(3, newRecord.getSurname());
        statement.setString(4, newRecord.getEmail());
        statement.setString(5, newRecord.getPhone());
    }

    public User getUserByEmailAndPassword(final String email, final String password) {
        final String queryString = UserQueryStringBuilder.builder()
                .getAllUser()
                .where()
                .email()
                .and()
                .password()
                .build();
        try (final Connection connection = DriverManager.getConnection(urlConnectDB, dbUser, dbPassword);
             final PreparedStatement statement = connection.prepareStatement(queryString)) {
            statement.setString(1, email);
            statement.setString(2, password);
            try (final ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return recordMapper.mapRow(resultSet);
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new UserDAOException("Ошибка при получении пользователя.", e);
        }
    }

    public User getUserByEmail(final String email) {
        final String queryString = UserQueryStringBuilder.builder()
                .getAllUser()
                .where()
                .email()
                .build();
        try (final Connection connection = DriverManager.getConnection(urlConnectDB, dbUser, dbPassword);
             final PreparedStatement statement = connection.prepareStatement(queryString)) {
            statement.setString(1, email);
            try (final ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return recordMapper.mapRow(resultSet);
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new UserDAOException(e);
        }
    }

    public int getCountUser() {
        final String queryString = UserQueryStringBuilder.builder()
                .countRecord()
                .build();
        try (final Connection connection = DriverManager.getConnection(urlConnectDB, dbUser, dbPassword);
             final Statement statement = connection.createStatement();
             final ResultSet rs = statement.executeQuery(queryString)) {
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            throw new UserDAOException(e);
        }
    }
}
