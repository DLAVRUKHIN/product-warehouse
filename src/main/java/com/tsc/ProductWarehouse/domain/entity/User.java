package com.tsc.ProductWarehouse.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tsc.ProductWarehouse.domain.enums.UserRole;

import java.util.Base64;

/**
 * Класс описывающий сущность пользователя
 *
 * @author Дмитрий Лаврухин
 */
public class User {

    private int id;
    private UserRole userRole = UserRole.CLIENT;
    private String name;
    private String surname;
    private String email;
    private String phone;
    private String password;

    public User() {
    }

    public User(int id, UserRole userRole, String name, String surname, String email, String phone) {
        this.id = id;
        this.userRole = userRole;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "userType " + this.userRole +
                " name " + this.name +
                " surname " + this.surname +
                " email " + this.email +
                " phone " + this.phone +
                " password " + this.password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = Base64.getEncoder().encodeToString(password.getBytes());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
