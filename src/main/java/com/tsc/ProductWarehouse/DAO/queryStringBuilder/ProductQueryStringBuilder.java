package com.tsc.ProductWarehouse.DAO.queryStringBuilder;

/**
 * Класс для создания строки SQL запроса, для получения данных из БД о продуктах.
 *
 * @author Дмитрий Лаврухин
 */
public class ProductQueryStringBuilder {

    private final static String queryAllProduct = "SELECT * FROM product";
    private final static String delete = "DELETE FROM product";
    private final static String insert = "INSERT INTO product VALUES(DEFAULT, ?,?,?,?,?)";
    private final static String update = "UPDATE product set name = ?, type = ?, " +
            "price = ?, creation_date = ?, expiration_date = ?";
    private final static String where = " WHERE ";
    private final static String and = " AND ";
    private final static String or = " OR ";
    private final static String id = "id = ?";
    private final static String type = "type = ?";
    private final static String name = "name = ?";
    private final static String fresh = "expiration_date > ?";

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final StringBuilder queryString;

        public Builder() {
            queryString = new StringBuilder();
        }

        public Builder queryAllProduct() {
            this.queryString.append(queryAllProduct);
            return this;
        }

        public Builder delete() {
            this.queryString.append(delete);
            return this;
        }

        public Builder insert() {
            this.queryString.append(insert);
            return this;
        }

        public Builder update() {
            this.queryString.append(update);
            return this;
        }

        public Builder where() {
            this.queryString.append(where);
            return this;
        }

        public Builder or() {
            this.queryString.append(or);
            return this;
        }

        public Builder and() {
            this.queryString.append(and);
            return this;
        }

        public Builder id() {
            this.queryString.append(id);
            return this;
        }

        public Builder type() {
            this.queryString.append(type);
            return this;
        }

        public Builder name() {
            this.queryString.append(name);
            return this;
        }

        public Builder onlyFresh() {
            this.queryString.append(fresh);
            return this;
        }

        public Builder pageNumber(int pageNumber, int pageSize) {
            this.queryString.append(createPaginationString(pageNumber, pageSize));
            return this;
        }

        private String createPaginationString(int pageNumber, int pageSize) {
            if (pageNumber <= 0) {
                pageNumber = 1;
            }
            int offset = (pageNumber - 1) * pageSize;
            return " LIMIT " + pageSize + " OFFSET " + offset;
        }

        public String build() {
            return queryString.toString();
        }
    }
}
