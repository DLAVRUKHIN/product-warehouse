package com.tsc.ProductWarehouse.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.ProductWarehouse.DAO.impl.ProductDAO;
import com.tsc.ProductWarehouse.domain.entity.Product;
import com.tsc.ProductWarehouse.service.exception.ProductServiceException;

import java.util.Map;
import java.util.Optional;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class ProductService {

    ProductDAO dao = ProductDAO.getInstance();
    final ObjectMapper objectMapper = new ObjectMapper();

    public String getProductsByParameter(Map<String, String[]> parameters) throws JsonProcessingException {
        Optional<String[]> idString = Optional.ofNullable(parameters.get("id"));
        Optional<String[]> name = Optional.ofNullable(parameters.get("name"));
        Optional<String[]> type = Optional.ofNullable(parameters.get("type"));
        Optional<String[]> onlyFresh = Optional.ofNullable(parameters.get("onlyFresh"));
        Optional<String[]> pageString = Optional.ofNullable(parameters.get("page"));
        Optional<String[]> pageSizeString = Optional.ofNullable(parameters.get("pageSize"));

        if (parameters.size() == 0) {
            final int page = pageString
                    .map(e -> Integer.parseInt(e[0]))
                    .orElse(1);
            final int pageSize = pageSizeString
                    .map(e -> Integer.parseInt(e[0]))
                    .orElse(5);
            return createJsonStringFromProduct(dao.getAll(page, pageSize));
        } else if (idString.isPresent() && nonNull(idString.get()[0])) {
            final int id = pageString
                    .map(e -> Integer.parseInt(e[0]))
                    .orElseThrow(() -> new ProductServiceException("Ошибка в параметре id."));
            return createJsonStringFromProduct(dao.getById(id));
        } else if (name.isPresent() && nonNull(name.get()[0])) {
            return createJsonStringFromProduct(dao.getProductByProductName(name.get()[0]));
        } else if (type.isPresent() && nonNull(type.get()[0])) {
            return createJsonStringFromProduct(dao.getProductByProductType(type.get()[0]));
        } else if (onlyFresh.isPresent()) {
            return createJsonStringFromProduct(dao.getFreshProducts());
        } else {
            throw new ProductServiceException("Передан невалидный список параметров.");
        }
    }

    public int createProduct(Product product) {
        checkProduct(product);
        return dao.insertRecord(product);
    }

    public void updateProduct(Product product, int id) {
        Optional<Product> existProduct = Optional.ofNullable(dao.getById(id));
        if (existProduct.isPresent()) {
            dao.updateRecord(id, product);
        } else {
            throw new ProductServiceException("Пользователя с id = " + id + "не существует.");
        }
    }

    public void deleteProduct(int id) {
        dao.deleteRecord(id);
    }

    private void checkProduct(Product product) {
        if (isNull(product.getName())) {
            throw new ProductServiceException("Название продукта не может быть пустым.");
        }
    }

    private String createJsonStringFromProduct(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }
}
