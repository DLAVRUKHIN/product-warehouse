package com.tsc.ProductWarehouse.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.ProductWarehouse.DAO.impl.UserDAO;
import com.tsc.ProductWarehouse.domain.entity.User;
import com.tsc.ProductWarehouse.domain.enums.UserRole;
import com.tsc.ProductWarehouse.service.exception.UserServiceException;
import com.tsc.ProductWarehouse.util.JWTUtil;

import java.util.Map;
import java.util.Optional;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class UserService {

    private final UserDAO dao = UserDAO.getInstance();
    final ObjectMapper objectMapper = new ObjectMapper();

    public String getUsersByParameter(Map<String, String[]> parameter) throws JsonProcessingException {
        Optional<String[]> idString = Optional.ofNullable(parameter.get("id"));
        Optional<String[]> email = Optional.ofNullable(parameter.get("email"));
        Optional<String[]> pageString = Optional.ofNullable(parameter.get("page"));
        Optional<String[]> pageSizeString = Optional.ofNullable(parameter.get("pageSize"));

        if (parameter.size() == 0) {
            final int page = pageString
                    .map(e -> Integer.parseInt(e[0]))
                    .orElse(1);
            final int pageSize = pageSizeString
                    .map(e -> Integer.parseInt(e[0]))
                    .orElse(5);
            return createJsonStringFromUser(dao.getAll(page, pageSize));
        } else if (idString.isPresent() && nonNull(idString.get()[0])) {
            final int id = pageString
                    .map(e -> Integer.parseInt(e[0]))
                    .orElseThrow(() -> new UserServiceException("Ошибка в параметре id."));
            return createJsonStringFromUser(dao.getById(id));
        } else if (email.isPresent() && nonNull(email.get()[0])) {
            return createJsonStringFromUser(dao.getUserByEmail(email.get()[0]));
        } else {
            throw new UserServiceException("Передан невалидный список параметров.");
        }
    }

    public int createUser(User user) {
        checkUserParameter(user);
        if (isNull(dao.getUserByEmail(user.getEmail()))) {
            return dao.insertRecord(user);
        } else {
            throw new UserServiceException("Пользователь с таким email уже существует.");
        }
    }

    public void updateUser(User user, int id) {
        Optional<User> existUser = Optional.ofNullable(dao.getById(id));
        if (existUser.isPresent()) {
            dao.updateRecord(id, user);
        } else {
            throw new UserServiceException("Пользователя с id = " + id + "не существует.");
        }
    }

    public void deleteUser(int id) {
        dao.deleteRecord(id);
    }

    public String signUpUser(User user) {
        checkUserParameter(user);
        checkExistUser(user);
        user.setUserRole(getUserRole());
        final int id = dao.insertRecord(user);
        return JWTUtil.createJWT(Integer.toString(id), user.getEmail(), user.getUserRole());
    }

    public String userLoginJwt(User user) {
        checkUserParameter(user);
        User userFromDB = dao.getUserByEmailAndPassword(user.getEmail(), user.getPassword());
        if (nonNull(userFromDB)) {
            return JWTUtil.createJWT(Integer.toString(userFromDB.getId()), userFromDB.getEmail(), userFromDB.getUserRole());
        } else {
            throw new UserServiceException("Пользователь с таким email/паролем не найден");
        }
    }

    private String createJsonStringFromUser(Object obj) throws JsonProcessingException {
        return objectMapper.writeValueAsString(obj);
    }

    private void checkUserParameter(User user) {
        if (isNull(user.getEmail()) && isNull(user.getPassword())) {
            throw new UserServiceException("Email и пароль не могут быть пустыми.");
        }
    }

    private void checkExistUser(User user) {
        if (nonNull(dao.getUserByEmail(user.getEmail()))) {
            throw new UserServiceException("Пользователь с таким email уже существует.");
        }
    }

    private UserRole getUserRole() {
        final int countUser = dao.getCountUser();
        if (countUser == 0) {
            return UserRole.ADMIN;
        } else {
            return UserRole.CLIENT;
        }
    }
}
