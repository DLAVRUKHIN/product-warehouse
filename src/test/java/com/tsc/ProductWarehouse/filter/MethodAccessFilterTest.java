package com.tsc.ProductWarehouse.filter;

import com.tsc.ProductWarehouse.util.JWTUtil;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class MethodAccessFilterTest {
    private final String userIdInJwt = "12";

    private final String userAdminToken = "Bearer eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJzdWIiOiIxMiIsImVtYWlsIjoi" +
            "MTIzQG1haWwucnUiLCJyb2wiOiJBRE1JTiJ9.1Db3KHqPOEyBkZSfE6SEvwUhuZ452qz1fV_rMoVrGPu1zV0ZbY7FwcKgzfBQhHm_F6nf" +
            "yTS-DiFOtyrBOVi9Ew";

    private final String userClientJwt = "Bearer eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJzdWIiOiIxMiIsImVtYWlsIjoiM" +
            "TIzQG1haWwucnUiLCJyb2wiOiJDTElFTlQifQ.UwVMMbMTUa4BoQsx9WwY0pw_iTsOfmzgb9aJE_Rz1ub5Es-fHwrR-8awy3qcm3NR_ND" +
            "uCeL-FBYWpEypwu2B3w";

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private FilterChain filterChain;

    private StringWriter stringWriter;
    private PrintWriter writer;

    @BeforeEach
    void setUp() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        filterChain = mock(FilterChain.class);
        stringWriter = new StringWriter();
        writer = new PrintWriter(stringWriter);
    }

    @Test
    void doFilter_shouldInvokeDoFilter_WhenUserIsAdmin() throws ServletException, IOException {
        when(request.getHeader(JWTUtil.TOKEN_HEADER)).thenReturn(userAdminToken);
        when(request.getMethod()).thenReturn("GET");
        when(request.getRequestURL()).thenReturn(new StringBuffer("app/product"));
        new MethodAccessFilter().doFilter(request, response, filterChain);
        verify(filterChain).doFilter(request, response);
    }

    @Test
    void doFilter_shouldInvokeDoFilter_WhenUserIsClientAndHasAccessToMethod() throws ServletException, IOException {
        when(response.getWriter()).thenReturn(writer);
        when(request.getHeader(JWTUtil.TOKEN_HEADER)).thenReturn(userClientJwt);
        when(request.getMethod()).thenReturn("PUT");
        when(request.getRequestURL()).thenReturn(new StringBuffer("app/product"));

        new MethodAccessFilter().doFilter(request, response, filterChain);
        verify(response).setStatus(403);
        assertThat(stringWriter.toString()).contains("Не достаточно прав для выполнения операции");
    }

    @Test
    void doFilter_ShouldReturnErrorMessageAndErrorCode_WhenJWTNotExist() throws IOException, ServletException {
        when(response.getWriter()).thenReturn(writer);
        when(request.getHeader(JWTUtil.TOKEN_HEADER)).thenReturn(null);

        new MethodAccessFilter().doFilter(request, response, filterChain);
        verify(response).setStatus(401);
        assertThat(stringWriter.toString()).contains("Необходимо предоставить валидный JWT токен");
    }

    @Test
    void doFilter_ShouldReturnErrorMessageAndErrorCode_WhenMethodsNotSupport() throws IOException, ServletException {
        when(response.getWriter()).thenReturn(writer);
        when(request.getHeader(JWTUtil.TOKEN_HEADER)).thenReturn(userAdminToken);
        when(request.getMethod()).thenReturn("HEADER");
        when(request.getRequestURL()).thenReturn(new StringBuffer("app/product"));

        new MethodAccessFilter().doFilter(request, response, filterChain);
        verify(response).setStatus(405);
        assertThat(stringWriter.toString()).contains("Метод не поддерживается");
    }
}