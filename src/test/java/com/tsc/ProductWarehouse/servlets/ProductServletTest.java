package com.tsc.ProductWarehouse.servlets;

import com.tsc.ProductWarehouse.DAO.exceptions.ProductDAOException;
import com.tsc.ProductWarehouse.DAO.impl.ProductDAO;
import com.tsc.ProductWarehouse.domain.entity.Product;
import com.tsc.ProductWarehouse.domain.enums.ProductType;
import com.tsc.ProductWarehouse.service.ProductService;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class ProductServletTest {

    private final String validProductJson = "{\n" +
            "    \"productType\":\"CHEESE\",\n" +
            "    \"name\":\"test product\",\n" +
            "    \"price\":\"1111\",\n" +
            "    \"expirationDate\":\"24-09-2021\"\n" +
            "}";

    private final String notValidProductJson = "{\n" +
            "    \"productType111\":\"CHEESE\",\n" +
            "    \"name111\":\"test product\",\n" +
            "    \"price111\":\"1111\",\n" +
            "    \"expirationDate111\":\"24-09-2021\"\n" +
            "}";
    @Mock
    private ProductDAO dao;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private ProductService productService;
    private StringWriter stringWriter;
    private PrintWriter writer;

    @BeforeEach
    void setUp() throws IOException {
        stringWriter = new StringWriter();
        writer = new PrintWriter(stringWriter);
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        dao = mock(ProductDAO.class);
        productService = mock(ProductService.class);
        when(response.getWriter()).thenReturn(writer);
    }


    @Test
    void doGet_mustReturnProductsByDefaultPageSizeAndPageNumber_whenRequestNotContainsParameter() throws IOException, ServletException {
        final String exceptProductList = "[{\"id\":1,\"productType\":\"BREAD\",\"name\":\"test product\",\"price\":22.11,\"creationDate\":\"09-01-2021\",\"expirationDate\":\"10-01-2021\"}," +
                "{\"id\":2,\"productType\":\"BREAD\",\"name\":\"test product\",\"price\":22.11,\"creationDate\":\"09-01-2021\",\"expirationDate\":\"10-01-2021\"}," +
                "{\"id\":3,\"productType\":\"MILK\",\"name\":\"test product\",\"price\":22.11,\"creationDate\":\"09-01-2021\",\"expirationDate\":\"10-01-2021\"}," +
                "{\"id\":4,\"productType\":\"MILK\",\"name\":\"test product\",\"price\":22.11,\"creationDate\":\"09-01-2021\",\"expirationDate\":\"10-01-2021\"}," +
                "{\"id\":5,\"productType\":\"CHEESE\",\"name\":\"test product\",\"price\":22.11,\"creationDate\":\"09-01-2021\",\"expirationDate\":\"10-01-2021\"}]";
        final List<Product> productList = new ArrayList<>();
        productList.add(new Product(1, ProductType.BREAD, "test product", 22.11, LocalDate.of(2021, 1, 9), LocalDate.of(2021, 1, 10)));
        productList.add(new Product(2, ProductType.BREAD, "test product", 22.11, LocalDate.of(2021, 1, 9), LocalDate.of(2021, 1, 10)));
        productList.add(new Product(3, ProductType.MILK, "test product", 22.11, LocalDate.of(2021, 1, 9), LocalDate.of(2021, 1, 10)));
        productList.add(new Product(4, ProductType.MILK, "test product", 22.11, LocalDate.of(2021, 1, 9), LocalDate.of(2021, 1, 10)));
        productList.add(new Product(5, ProductType.CHEESE, "test product", 22.11, LocalDate.of(2021, 1, 9), LocalDate.of(2021, 1, 10)));
        final HashMap<String, String[]> requestMap = new HashMap<>();

        when(request.getParameterMap()).thenReturn(requestMap);
        when(dao.getAll(anyInt(), anyInt())).thenReturn(productList);

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doGet(request, response);
            verify(response).setStatus(200);
            verify(dao).getAll(1, 5);
            assertThat(stringWriter.toString()).isEqualTo(exceptProductList);
        }
    }

    @Disabled
    @Test
    void doGet_mustReturnProducts_whenParameterContainsPageNumberAndPageSize() throws IOException, ServletException {
        final String pageNumber = "6";
        final String pageSize = "9";
        final HashMap<String, String[]> requestMap = new HashMap<>();
        requestMap.put("page", new String[]{pageNumber});
        requestMap.put("pageSize", new String[]{pageSize});

        when(request.getParameterMap()).thenReturn(requestMap);
        when(request.getParameter("page")).thenReturn(pageNumber);
        when(request.getParameter("pageSize")).thenReturn(pageSize);

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doGet(request, response);
            verify(response).setStatus(200);
            verify(dao).getAll(Integer.parseInt(pageNumber), Integer.parseInt(pageSize));
        }
    }

    @Disabled
    @Test
    void doGet_mustReturnProducts_whenRequestContainsIdParameter() throws IOException, ServletException {
        final String exceptProductString = "{\"id\":1,\"productType\":\"CHEESE\",\"name\":\"test product\",\"price\":1111.0," +
                "\"creationDate\":\"09-01-2021\",\"expirationDate\":\"10-01-2021\"}";
        final String id = "1";
        final HashMap<String, String[]> requestMap = new HashMap<>();
        requestMap.put("id", new String[]{id});
        final Product productDB = new Product(1, ProductType.CHEESE, "test product", 1111.00,
                LocalDate.of(2021, 1, 10), LocalDate.of(2021, 1, 11));

        when(request.getParameterMap()).thenReturn(requestMap);
        when(request.getParameter("id")).thenReturn(id);
        when(dao.getById(anyInt())).thenReturn(productDB);

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doGet(request, response);
            verify(dao).getById(Integer.parseInt(id));
            verify(response).setStatus(200);
            assertThat(stringWriter.toString()).isEqualTo(exceptProductString);
        }
    }

    @Test
    void doGet_mustReturnProducts_whenRequestContainsNameParameter() throws IOException, ServletException {
        final String name = "test product";
        final String exceptProductList = "[{\"id\":1,\"productType\":\"BREAD\",\"name\":\"test product\",\"price\":22.11,\"creationDate\":\"09-01-2021\",\"expirationDate\":\"10-01-2021\"}," +
                "{\"id\":2,\"productType\":\"BREAD\",\"name\":\"test product\",\"price\":22.11,\"creationDate\":\"09-01-2021\",\"expirationDate\":\"10-01-2021\"}]";
        final List<Product> productList = new ArrayList<>();
        productList.add(new Product(1, ProductType.BREAD, "test product", 22.11, LocalDate.of(2021, 1, 9), LocalDate.of(2021, 1, 10)));
        productList.add(new Product(2, ProductType.BREAD, "test product", 22.11, LocalDate.of(2021, 1, 9), LocalDate.of(2021, 1, 10)));
        final HashMap<String, String[]> parameterMap = new HashMap<>();
        parameterMap.put("name", new String[]{name});

        when(dao.getProductByProductName(name)).thenReturn(productList);
        when(request.getParameterMap()).thenReturn(parameterMap);
        when(request.getParameter("name")).thenReturn(name);

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doGet(request, response);
            verify(dao).getProductByProductName(name);
            verify(response).setStatus(200);
            assertThat(stringWriter.toString()).isEqualTo(exceptProductList);
        }
    }

    @Test
    void doGet_mustReturnProducts_whenRequestContainsTypeParameter() throws IOException, ServletException {
        final String type = "bread";
        final String exceptProductList = "[{\"id\":1,\"productType\":\"BREAD\",\"name\":\"test product\",\"price\":22.11,\"creationDate\":\"09-01-2021\",\"expirationDate\":\"10-01-2021\"}," +
                "{\"id\":2,\"productType\":\"BREAD\",\"name\":\"test product\",\"price\":22.11,\"creationDate\":\"09-01-2021\",\"expirationDate\":\"10-01-2021\"}]";
        final List<Product> productList = new ArrayList<>();
        productList.add(new Product(1, ProductType.BREAD, "test product", 22.11, LocalDate.of(2021, 1, 9), LocalDate.of(2021, 1, 10)));
        productList.add(new Product(2, ProductType.BREAD, "test product", 22.11, LocalDate.of(2021, 1, 9), LocalDate.of(2021, 1, 10)));
        when(dao.getProductByProductType(type)).thenReturn(productList);
        final HashMap<String, String[]> parameterMap = new HashMap<>();
        parameterMap.put("type", new String[]{type});

        when(request.getParameterMap()).thenReturn(parameterMap);
        when(request.getParameter("type")).thenReturn(type);

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doGet(request, response);
            verify(dao).getProductByProductType(type);
            verify(response).setStatus(200);
            assertThat(stringWriter.toString()).isEqualTo(exceptProductList);
        }
    }

    @Disabled
    @Test
    void doGet_mustReturnProducts_whenRequestContainsOnlyFreshParameter() throws IOException, ServletException {
        final String exceptProductList = "[{\"id\":1,\"productType\":\"BREAD\",\"name\":\"test product\",\"price\":22.11,\"creationDate\":\"09-01-2021\",\"expirationDate\":\"10-01-2021\"}," +
                "{\"id\":2,\"productType\":\"BREAD\",\"name\":\"test product\",\"price\":22.11,\"creationDate\":\"09-01-2021\",\"expirationDate\":\"10-01-2021\"}]";
        final List<Product> productList = new ArrayList<>();
        productList.add(new Product(1, ProductType.BREAD, "test product", 22.11, LocalDate.of(2021, 1, 10), LocalDate.of(2021, 1, 11)));
        productList.add(new Product(2, ProductType.BREAD, "test product", 22.11, LocalDate.of(2021, 1, 10), LocalDate.of(2021, 1, 11)));
        final HashMap<String, String[]> parameterMap = new HashMap<>();
        parameterMap.put("onlyFresh", null);

        when(dao.getFreshProducts()).thenReturn(productList);
        when(request.getParameterMap()).thenReturn(parameterMap);

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doGet(request, response);
            verify(dao).getFreshProducts();
            verify(response).setStatus(200);
            assertThat(stringWriter.toString()).isEqualTo(exceptProductList);
        }
    }

    @Disabled
    @Test
    void doGet_mustReturnErrorMessage_whenParameterIdNotValid() throws IOException, ServletException {
        final String id = "1asd";
        HashMap<String, String[]> parameterMap = new HashMap<>();
        parameterMap.put("id", new String[]{id});
        when(request.getParameter("id")).thenReturn(id);
        when(request.getParameterMap()).thenReturn(parameterMap);
        new ProductServlet().doGet(request, response);
        verify(response).setStatus(500);
        assertThat(stringWriter.toString()).contains("Ошибка в параметре id = " + id);
    }

    @Test
    void doGet_mustReturnErrorMessage_whenDAOThrowException() throws IOException, ServletException {
        final String id = "1";
        final HashMap<String, String[]> parameterMap = new HashMap<>();
        parameterMap.put("id", new String[]{id});

        when(request.getParameter("id")).thenReturn(id);
        when(request.getParameterMap()).thenReturn(parameterMap);
        when(dao.getById(anyInt())).thenThrow(ProductDAOException.class);

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doGet(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Ошибка при получении информации о продуктах");
        }
    }

    @Test
    void doPost_mustReturnCode204_WhenJsonValid() throws IOException, ServletException {
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(validProductJson)));
        when(dao.insertRecord(any())).thenReturn(1);

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doPost(request, response);
            verify(response).setStatus(204);
        }
    }

    @Test
    void doPost_mustReturnErrorMessage_WhenJsonNotValid() throws IOException, ServletException {
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(notValidProductJson)));
        when(dao.insertRecord(any())).thenReturn(1);

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doPost(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Ошибка во входном сообщении. " +
                    "Проверьте правильность названия переданных полей.");
        }
    }

    @Test
    void doPost_mustReturnErrorMessage_whenDAOThrowException() throws IOException, ServletException {
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(validProductJson)));
        when(dao.insertRecord(any())).thenThrow(ProductDAOException.class);

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doPost(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Ошибка при создании продукта");
        }
    }

    @Disabled
    @Test
    void doPut_mustReturnCode201_whenIdAndJsonValid() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1");
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(validProductJson)));
        doNothing().when(dao).updateRecord(anyInt(), any());
        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doPut(request, response);
            verify(response).setStatus(201);
        }
    }

    @Test
    void doPut_mustErrorMessage_whenIdNotValid() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1asd");
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(validProductJson)));
        doNothing().when(dao).updateRecord(anyInt(), any());

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doPut(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Ошибка в параметре id");
        }
    }

    @Test
    void doPut_mustErrorMessage_whenJsonNotValid() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1");
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(notValidProductJson)));
        doNothing().when(dao).updateRecord(anyInt(), any());

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doPut(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Ошибка во входном сообщении. " +
                    "Проверьте правильность названия переданных полей.");
        }
    }

    @Disabled
    @Test
    void doPut_mustErrorMessage_whenDAOThrowException() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1");
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(validProductJson)));
        doThrow(ProductDAOException.class).when(dao).updateRecord(anyInt(), any());

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doPut(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Ошибка при обновлении данных продукта");
        }
    }

    @Test
    void doDelete_mustReturnCode204_whenParameterIdValid() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1");
        doNothing().when(dao).deleteRecord(anyInt());

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doDelete(request, response);
            verify(response).setStatus(204);
            assertThat(stringWriter.toString()).isEmpty();
        }
    }

    @Test
    void doDelete_mustReturnErrorMessage_whenParameterIdNotValid() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1asd");
        doNothing().when(dao).deleteRecord(anyInt());

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doDelete(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Ошибка в параметре id");
        }
    }

    @Test
    void doDelete_mustReturnErrorMessage_whenDAOThrowException() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1");
        doThrow(ProductDAOException.class).when(dao).deleteRecord(anyInt());

        try (final MockedStatic<ProductDAO> theMock = Mockito.mockStatic(ProductDAO.class)) {
            theMock.when(ProductDAO::getInstance).thenReturn(dao);
            new ProductServlet().doDelete(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Не удалось удалить продукт c id = 1");
        }
    }
}