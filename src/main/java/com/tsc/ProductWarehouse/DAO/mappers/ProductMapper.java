package com.tsc.ProductWarehouse.DAO.mappers;

import com.tsc.ProductWarehouse.DAO.interfaces.RowMapper;
import com.tsc.ProductWarehouse.domain.entity.Product;
import com.tsc.ProductWarehouse.domain.enums.ProductType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.time.LocalDate;

/**
 * Маппер для продукта
 *
 * @author Дмитрий Лаврухин
 */
public class ProductMapper implements RowMapper<Product> {

    @Override
    public Product mapRow(final ResultSet resultSet) throws SQLException {
        final int id = resultSet.getInt("id");
        final ProductType productType = ProductType.valueOf(resultSet.getString("type"));
        final String name = resultSet.getString("name");
        final double price = resultSet.getDouble("price");
        final LocalDate creationDate = resultSet.getDate("creation_date").toLocalDate();
        final LocalDate expirationDate = resultSet.getDate("expiration_date").toLocalDate();
        return new Product(id, productType, name, price, creationDate, expirationDate);
    }
}
