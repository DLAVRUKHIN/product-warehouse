package com.tsc.ProductWarehouse.servlets;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.ProductWarehouse.DAO.exceptions.UserDAOException;
import com.tsc.ProductWarehouse.domain.entity.User;
import com.tsc.ProductWarehouse.domain.enums.UserRole;
import com.tsc.ProductWarehouse.filter.annotation.PreAuthorization;
import com.tsc.ProductWarehouse.service.UserService;
import com.tsc.ProductWarehouse.service.exception.UserServiceException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

/**
 * Сервлет для CRUD операций с пользователем.
 *
 * @author Дмитрий Лаврухин
 */
public class UserServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger();
    private final UserService service = new UserService();
    private final ObjectMapper mapper = new ObjectMapper();

    @PreAuthorization(roles = {UserRole.ADMIN, UserRole.CLIENT})
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        final Map<String, String[]> requestParameterMap = request.getParameterMap();
        try {
            String userListString = service.getUsersByParameter(requestParameterMap);
            response.setStatus(200);
            response.getWriter().write(userListString);
        } catch (UserDAOException | UserServiceException e) {
            logger.error("Ошибка при получении информации о пользователе. " + e.getMessage());
            writeToResponseErrorCodeAndMessage(response, 500, "Ошибка при получении " +
                    "информации о пользователях");
        } catch (NumberFormatException e) {
            logger.error("Ошибка при получении информации о пользователе. " +
                    "Ошибка в параметре id = " + request.getParameter("id") + ". " + e.getMessage());
            writeToResponseErrorCodeAndMessage(response, 500, "Ошибка в параметре id = " +
                    request.getParameter("id"));
        }
    }

    @PreAuthorization(roles = {UserRole.ADMIN})
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            final Optional<User> user = createUserByRequest(request);
            if (user.isPresent()) {
                service.createUser(user.get());
            } else {
                logger.error("Ошибка при создании пользователя");
                writeToResponseErrorCodeAndMessage(response, 500, "Ошибка при создании пользователя" +
                        " Проверьте правильность переданных данных.");
            }
        } catch (UserDAOException e) {
            logger.error("Не удалось создать пользователя" + e.getMessage());
            writeToResponseErrorCodeAndMessage(response, 500, "Ошибка при создании пользователя");
        } catch (UserServiceException e) {
            logger.error("Ошибка при создании пользователя. " + e.getMessage());
            writeToResponseErrorCodeAndMessage(response, 500, "Пользователь с таким email уже существует");
        } catch (JacksonException e) {
            logger.error("Ошибка при создании объекта User из входящего сообщения." + e.getMessage());
            writeToResponseErrorCodeAndMessage(response, 500, "Ошибка во входном сообщении. " +
                    "Проверьте правильность переданных данных.");
        }
    }

    @PreAuthorization(roles = {UserRole.ADMIN, UserRole.CLIENT})
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            final int id = Integer.parseInt(req.getParameter("id"));
            final Optional<User> user = createUserByRequest(req);
            if (user.isPresent()) {
                service.updateUser(user.get(), id);
                resp.setStatus(201);
                logger.info("Обновление данных пользователя с id = " + id);
            } else {
                logger.error("Ошибка при обновлении данных пользователя c id = " + id);
                writeToResponseErrorCodeAndMessage(resp, 500, "Ошибка при обновлении " +
                        "данных пользователя. Проверьте корректность переданных данных.");
            }
        } catch (NumberFormatException e) {
            logger.error("Не удалось обновить данные о пользователе. Ошибка в параметре id = "
                    + req.getParameter("id") + " " + e.getMessage());
            writeToResponseErrorCodeAndMessage(resp, 500, "Ошибка в параметре id");
        } catch (UserDAOException | UserServiceException e) {
            logger.error("Не удалось обновить данные о пользователе c id = "
                    + req.getParameter("id") + " " + e.getMessage());
            writeToResponseErrorCodeAndMessage(resp, 500, "Ошибка при обновлении " +
                    "данных о пользователе");
        } catch (JacksonException e) {
            logger.error("Ошибка при создании объекта User из входящего сообщения." + e.getMessage());
            writeToResponseErrorCodeAndMessage(resp, 500, "Ошибка во входном сообщении. " +
                    "Проверьте правильность названия переданных полей.");
        }
    }

    @PreAuthorization(roles = {UserRole.ADMIN})
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            final Optional<Integer> id = Optional.ofNullable(req.getParameter("id")).map(Integer::parseInt);
            if (id.isPresent()) {
                service.deleteUser(id.get());
                resp.setStatus(204);
                logger.info("Удаление пользователя с id = " + id);
            } else {
                logger.error("Для операции удаления не передан параметр id пользователя");
                writeToResponseErrorCodeAndMessage(resp, 500, "Для удаления пользователя передайте параметр id");
            }
        } catch (NumberFormatException e) {
            logger.error("Не удалось удалить пользователя. Ошибка в параметре id = " + req.getParameter("id"));
            writeToResponseErrorCodeAndMessage(resp, 500, "Ошибка в параметре id");
        } catch (UserDAOException e) {
            logger.error("Не удалось удалить пользователя c id = " + req.getParameter("id") +
                    ". " + e.getMessage());
            writeToResponseErrorCodeAndMessage(resp, 500, "Не удалось удалить пользователя " +
                    "c id = " + req.getParameter("id"));
        }
    }

    private Optional<User> createUserByRequest(final HttpServletRequest request) throws IOException {
        request.setCharacterEncoding("UTF-8");
        final User user = mapper.readValue(request.getReader(), User.class);
        return Optional.ofNullable(user);
    }

    private void writeToResponseErrorCodeAndMessage(final HttpServletResponse response,
                                                    final int errorCode,
                                                    final String errorMessage) throws IOException {
        response.setStatus(errorCode);
        response.getWriter().print("{" +
                "\"error message\": " +
                "\"" + errorMessage + "\"" +
                "}");
    }
}
