package com.tsc.ProductWarehouse.servlets;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.ProductWarehouse.DAO.exceptions.ProductDAOException;
import com.tsc.ProductWarehouse.domain.entity.Product;
import com.tsc.ProductWarehouse.domain.enums.UserRole;
import com.tsc.ProductWarehouse.filter.annotation.PreAuthorization;
import com.tsc.ProductWarehouse.service.ProductService;
import com.tsc.ProductWarehouse.service.exception.ProductServiceException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

/**
 * Сервлет для CRUD операций с продуктом.
 *
 * @author Дмитрий Лаврухин
 */
public class ProductServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger();
    private final ProductService productService = new ProductService();
    private final ObjectMapper mapper = new ObjectMapper();

    @PreAuthorization(roles = {UserRole.ADMIN, UserRole.CLIENT})
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        final Map<String, String[]> requestParameterMap = request.getParameterMap();
        try {
            String productListString = productService.getProductsByParameter(requestParameterMap);
            response.setStatus(200);
            response.getWriter().write(productListString);
        } catch (ProductDAOException | ProductServiceException e) {
            logger.error("Ошибка при получении информации о " +
                    "продуктах. " + e.getMessage());
            writeToResponseErrorCodeAndMessage(response, 500, "Ошибка при получении информации о " +
                    "продуктах.");
        } catch (NumberFormatException e) {
            logger.error("Ошибка при получении информации о продукте. " +
                    "Ошибка в параметре id = " + request.getParameter("id") + ". " + e.getMessage());
            writeToResponseErrorCodeAndMessage(response, 500, "Ошибка в параметре id = " + request.getParameter("id"));
        }
    }

    @PreAuthorization(roles = {UserRole.ADMIN})
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            final Optional<Product> product = createProductByRequest(request);
            if (product.isPresent()) {
                final int id = productService.createProduct(product.get());
                response.setStatus(204);
                logger.info("Создан продукт с id = " + id);
            } else {
                logger.error("Ошибка при создании продукта");
                writeToResponseErrorCodeAndMessage(response, 500, "Ошибка при создании продукта" +
                        " Проверьте корректность переданных данных.");
            }
        } catch (ProductDAOException | ProductServiceException e) {
            logger.error("Не удалось добавить продукт" + e.getMessage());
            writeToResponseErrorCodeAndMessage(response, 500, "Ошибка при создании продукта");
        } catch (JacksonException e) {
            logger.error("Ошибка при создании объекта Product из входящего сообщения." + e.getMessage());
            writeToResponseErrorCodeAndMessage(response, 500, "Ошибка во входном сообщении. " +
                    "Проверьте правильность названия переданных полей.");
        }
    }

    @PreAuthorization(roles = {UserRole.ADMIN})
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            final int id = Integer.parseInt(req.getParameter("id"));
            final Optional<Product> product = createProductByRequest(req);
            if (product.isPresent()) {
                productService.updateProduct(product.get(), id);
                resp.setStatus(201);
                logger.info("Обновлены данные продукта с id = " + id);
            } else {
                logger.error("Ошибка при обновлении информации продукта.");
                writeToResponseErrorCodeAndMessage(resp, 500, "Ошибка во входном сообщении. " +
                        "Проверьте правильность названия переданных полей.");
            }
        } catch (NumberFormatException e) {
            logger.error("Не удалось обновить данные продукта. Ошибка в параметре id = "
                    + req.getParameter("id") + " " + e.getMessage());
            writeToResponseErrorCodeAndMessage(resp, 500, "Ошибка в параметре id");
        } catch (ProductDAOException | ProductServiceException e) {
            logger.error("Не удалось обновить данные продукта c id = "
                    + req.getParameter("id") + " " + e.getMessage());
            writeToResponseErrorCodeAndMessage(resp, 500, "Ошибка при обновлении данных продукта");
        } catch (JacksonException e) {
            logger.error("Ошибка при создании объекта Product из входящего сообщения." + e.getMessage());
            writeToResponseErrorCodeAndMessage(resp, 500, "Ошибка во входном сообщении. " +
                    "Проверьте правильность названия переданных полей.");
        }
    }

    @PreAuthorization(roles = {UserRole.ADMIN})
    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            final Optional<Integer> id = Optional.ofNullable(req.getParameter("id")).map(Integer::parseInt);
            if (id.isPresent()) {
                productService.deleteProduct(id.get());
                resp.setStatus(204);
                logger.info("Удаление продукта с id = " + id);
            } else {
                logger.error("Для операции удаления не передан параметр id пользователя");
                writeToResponseErrorCodeAndMessage(resp, 500, "Для удаления пользователя передайте параметр id");
            }
            logger.info("Удален продукт с id = " + id);
        } catch (NumberFormatException e) {
            logger.error("Не удалось удалить продукт. Ошибка в параметре id = "
                    + req.getParameter("id") + " " + e.getMessage());
            writeToResponseErrorCodeAndMessage(resp, 500, "Ошибка в параметре id");
        } catch (ProductDAOException | ProductServiceException e) {
            logger.error("Не удалось удалить продукт c id = "
                    + req.getParameter("id") + " " + e.getMessage());
            writeToResponseErrorCodeAndMessage(resp, 500, "Не удалось удалить продукт c id = "
                    + req.getParameter("id"));
        }
    }

    private Optional<Product> createProductByRequest(final HttpServletRequest request) throws IOException {
        request.setCharacterEncoding("UTF-8");
        final Product product = mapper.readValue(request.getReader(), Product.class);
        return Optional.ofNullable(product);
    }

    private void writeToResponseErrorCodeAndMessage(final HttpServletResponse response,
                                                    final int errorCode,
                                                    final String errorMessage) throws IOException {
        response.setStatus(errorCode);
        response.getWriter().print("{" +
                "\"error message\": " +
                "\"" + errorMessage + "\"" +
                "}");
    }
}
