package com.tsc.ProductWarehouse.DAO.impl;

import com.tsc.ProductWarehouse.DAO.exceptions.DAOException;
import com.tsc.ProductWarehouse.DAO.interfaces.DAO;
import com.tsc.ProductWarehouse.DAO.interfaces.ListRowMapper;
import com.tsc.ProductWarehouse.DAO.interfaces.RowMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;

/**
 * Абстрактный класс, реализующий загрузку параметров подключения к БД, а так же содержащий
 * реализацию общих DAO методов.
 *
 * @param <T> тип объекта
 * @author Дмитрий Лаврухин
 */
public abstract class DefaultDao<T> implements DAO<T> {
    protected static final Logger logger = LogManager.getLogger();
    protected final String urlConnectDB;
    protected final String dbUser;
    protected final String dbPassword;
    protected final RowMapper<T> recordMapper;
    protected final ListRowMapper<T> listMapper;

    public DefaultDao(RowMapper<T> recordMapper, ListRowMapper<T> listMapper) {
        try (InputStream inputStream = DefaultDao.class
                .getClassLoader()
                .getResourceAsStream("application.properties")) {
            Class.forName("org.postgresql.Driver");
            Properties prop = new Properties();
            prop.load(inputStream);
            this.urlConnectDB = prop.getProperty("db.connection");
            this.dbUser = prop.getProperty("db.user");
            this.dbPassword = prop.getProperty("db.password");
            logger.info("Использование базы данных : " + urlConnectDB + ", пользователь " + dbUser);
            this.recordMapper = recordMapper;
            this.listMapper = listMapper;
        } catch (IOException e) {
            logger.error("Ошибка при чтении файла конфигурации " + e.getMessage());
            throw new DAOException();
        } catch (ClassNotFoundException e) {
            logger.error("Ошибка при загрузке драйвера базы данных. " + e.getMessage());
            throw new DAOException();
        }
    }

    protected T getById(final String queryString, final int id) {
        try (final Connection connection = DriverManager.getConnection(urlConnectDB, dbUser, dbPassword);
             final PreparedStatement statement = connection.prepareStatement(queryString)) {
            statement.setInt(1, id);
            try (final ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    return recordMapper.mapRow(resultSet);
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    protected List<T> getAll(final String queryString) {
        try (final Connection connection = DriverManager.getConnection(urlConnectDB, dbUser, dbPassword);
             final Statement statement = connection.createStatement();
             final ResultSet resultSet = statement.executeQuery(queryString)) {
            return listMapper.mapRow(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    protected void deleteRecord(final String queryString, final int id) {
        try (final Connection connection = DriverManager.getConnection(urlConnectDB, dbUser, dbPassword);
             final PreparedStatement statement = connection.prepareStatement(queryString)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
