package com.tsc.ProductWarehouse.DAO.interfaces;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Итрерфейс преобразования ResultSet к списку объектов
 *
 * @param <T> тип объекта
 * @author Дмитрий Лаврухин
 */
public interface ListRowMapper<T> {
    List<T> mapRow(ResultSet resultSet) throws SQLException;
}
