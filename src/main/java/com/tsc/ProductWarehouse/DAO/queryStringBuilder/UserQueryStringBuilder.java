package com.tsc.ProductWarehouse.DAO.queryStringBuilder;

/**
 * Класс для создания строки SQL запроса, для получения данных из БД о пользователях.
 *
 * @author Дмитрий Лаврухин
 */
public class UserQueryStringBuilder {
    private final static String queryAllUser = "SELECT * FROM users";
    private final static String delete = "DELETE FROM users";
    private final static String insert = "INSERT INTO users VALUES(DEFAULT, ?,?,?,?,?,?)";
    private final static String update = "UPDATE users set role = ?, name = ?, " +
            "surname = ?, email = ?, phone = ?";
    private final static String updatePassword = "UPDATE users set password = ?";
    private final static String countRecord = "SELECT COUNT(*) FROM users";
    private final static String where = " WHERE ";
    private final static String and = " AND ";
    private final static String or = " OR ";
    private final static String id = "id = ?";
    private final static String email = "email = ?";
    private final static String password = "password = ?";

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private final StringBuilder queryString;

        public Builder() {
            queryString = new StringBuilder();
        }

        public Builder getAllUser() {
            this.queryString.append(queryAllUser);
            return this;
        }

        public Builder delete() {
            this.queryString.append(delete);
            return this;
        }

        public Builder insert() {
            this.queryString.append(insert);
            return this;
        }

        public Builder update() {
            this.queryString.append(update);
            return this;
        }

        public Builder where() {
            this.queryString.append(where);
            return this;
        }

        public Builder or() {
            this.queryString.append(or);
            return this;
        }

        public Builder and() {
            this.queryString.append(and);
            return this;
        }

        public Builder id() {
            this.queryString.append(id);
            return this;
        }

        public Builder updatePassword() {
            this.queryString.append(updatePassword);
            return this;
        }

        public Builder countRecord() {
            this.queryString.append(countRecord);
            return this;
        }

        public Builder email() {
            this.queryString.append(email);
            return this;
        }

        public Builder password() {
            this.queryString.append(password);
            return this;
        }

        public Builder pageNumber(int pageNumber, int pageSize) {
            this.queryString.append(createPaginationString(pageNumber, pageSize));
            return this;
        }

        private String createPaginationString(int pageNumber, int pageSize) {
            if (pageNumber <= 0) {
                pageNumber = 1;
            }
            int offset = (pageNumber - 1) * pageSize;
            return " LIMIT " + pageSize + " OFFSET " + offset;
        }

        public String build() {
            return queryString.toString();
        }
    }
}
