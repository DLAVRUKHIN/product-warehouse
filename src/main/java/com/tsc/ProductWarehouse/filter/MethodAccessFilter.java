package com.tsc.ProductWarehouse.filter;

import com.tsc.ProductWarehouse.domain.enums.UserRole;
import com.tsc.ProductWarehouse.filter.annotation.PreAuthorization;
import com.tsc.ProductWarehouse.servlets.ProductServlet;
import com.tsc.ProductWarehouse.servlets.UserServlet;
import com.tsc.ProductWarehouse.util.JWTUtil;
import com.tsc.ProductWarehouse.util.exception.JWTException;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.Objects.nonNull;

/**
 * Фильтр для ограничения доступа к методам сервлетов, на основании аннотации PreAuthorization.
 * Для доступа к методам пользователям необходимо предоставить валидный JWT токен в header запроса.
 *
 * @author Дмитрий Лаврухин
 */
@WebFilter("/app/*")
public class MethodAccessFilter implements Filter {
    private final Logger logger = LogManager.getLogger();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        final String authKey = request.getHeader(JWTUtil.TOKEN_HEADER);
        if (nonNull(authKey)) {
            try {
                final UserRole role = JWTUtil.getRoleFormToken(authKey);
                final String servletMethod = getServletMethodByHttpMethodName(request.getMethod());
                final String url = request.getRequestURL().toString();
                final String id = JWTUtil.getUserIdFormToken(authKey);
                final boolean availabilityAccessRights = checkAccessMethod(url, servletMethod, role);

                if (availabilityAccessRights) {
                    logger.info("Доступ к методу " + request.getMethod() + " разрешен для пользователя с id = " + id);
                    filterChain.doFilter(servletRequest, servletResponse);
                } else {
                    logger.error("Отказано в доступе к" + request.getRequestURL() + " методу " + request.getMethod() +
                            " пользователю с id = " + id + " и ролью " + role);
                    writeToResponseErrorCodeAndMessage(response, 403, "Не достаточно прав " +
                            "для выполнения операции");
                }
            } catch (NoSuchMethodException e) {
                logger.error("Вызов неподдерживаемого метода " + request.getMethod());
                writeToResponseErrorCodeAndMessage(response, 405, "Метод не поддерживается");
            } catch (JWTException e) {
                logger.error("Предоставлен не валидный JWT токен. " + e.getMessage());
                writeToResponseErrorCodeAndMessage(response, 500,
                        "Предоставлен не валидный JWT токен.");
            }
        } else {
            logger.warn("Выполнен запрос без наличия JWT токена");
            writeToResponseErrorCodeAndMessage(response, 401, "Необходимо предоставить " +
                    "валидный JWT токен");
        }
    }

    private boolean checkAccessMethod(final String url, final String method, final UserRole role) throws NoSuchMethodException {
        if (url.endsWith("/user")) {
            final List<UserRole> roles = getListRolesMethodAccess(UserServlet.class, method);
            return roles.contains(role);
        } else if (url.endsWith("/product")) {
            final List<UserRole> roles = getListRolesMethodAccess(ProductServlet.class, method);
            return roles.contains(role);
        }
        return false;
    }

    private <T> List<UserRole> getListRolesMethodAccess(final Class<T> clazz, final String method) throws NoSuchMethodException {
        final Method clazzMethod = clazz.getDeclaredMethod(method, HttpServletRequest.class, HttpServletResponse.class);
        final UserRole[] roles = clazzMethod.getAnnotation(PreAuthorization.class).roles();
        return new ArrayList<>(Arrays.asList(roles));
    }

    private String getServletMethodByHttpMethodName(final String httpMethodName) throws NoSuchMethodException {
        switch (httpMethodName) {
            case "GET":
                return "doGet";
            case "POST":
                return "doPost";
            case "PUT":
                return "doPut";
            case "DELETE":
                return "doDelete";
            default:
                throw new NoSuchMethodException();
        }
    }

    private void writeToResponseErrorCodeAndMessage(final HttpServletResponse response,
                                                    final int errorCode,
                                                    final String errorMessage) throws IOException {
        response.setStatus(errorCode);
        response.getWriter().println("{" +
                "\"error message\": " +
                "\"" + errorMessage + "\"" +
                "}");
    }
}
