package com.tsc.ProductWarehouse.servlets;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.ProductWarehouse.DAO.exceptions.UserDAOException;
import com.tsc.ProductWarehouse.domain.entity.User;
import com.tsc.ProductWarehouse.service.UserService;
import com.tsc.ProductWarehouse.util.JWTUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Сервлет для регистрации нового пользователя.
 *
 * @author Дмитрий Лаврухин
 */
public class SignupServlet extends HttpServlet {
    private final Logger logger = LogManager.getLogger();
    private final UserService userService = new UserService();
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        try {
            final User user = objectMapper.readValue(request.getReader(), User.class);
            String jwt = userService.signUpUser(user);
            response.addHeader(JWTUtil.TOKEN_HEADER, jwt);
            response.setStatus(200);
            logger.info("Добавлен пользователь с email = " + user.getEmail());
        } catch (JsonProcessingException e) {
            logger.error("Не указан логин/пароль " + e.getMessage());
            writeToResponseErrorCodeAndMessage(response, 401, "Не указан логин/пароль");
        } catch (UserDAOException e) {
            logger.error("Ошибка при получении количества пользователей " + e.getMessage());
            writeToResponseErrorCodeAndMessage(response, 500, "Ошибка при получении количества пользователей");
        }
    }

    private void writeToResponseErrorCodeAndMessage(final HttpServletResponse response,
                                                    final int errorCode,
                                                    final String errorMessage) throws IOException {
        response.setStatus(errorCode);
        response.getWriter().println("{" +
                "\"error message\": " +
                "\"" + errorMessage + "\"" +
                "}");
    }
}
