package com.tsc.ProductWarehouse.servlets;

import com.tsc.ProductWarehouse.DAO.exceptions.UserDAOException;
import com.tsc.ProductWarehouse.DAO.impl.UserDAO;
import com.tsc.ProductWarehouse.domain.entity.User;
import com.tsc.ProductWarehouse.domain.enums.UserRole;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class UserServletTest {
    private final String validUserJson = "{\n" +
            "    \"email\" : \"testemail1111@mail.ru\",\n" +
            "    \"password\" : \"testpassword\",\n" +
            "    \"surname\":\"user surname\",\n" +
            "    \"name\":\"user name\",\n" +
            "    \"phone\":\"565443\"\n" +
            "}";

    private final String notValidUserJson = "{\n" +
            "    \"email11\" : \"testemail1111@mail.ru\",\n" +
            "    \"password11\" : \"testpassword\",\n" +
            "    \"surname1\":\"user surname\",\n" +
            "    \"name1\":\"user name\",\n" +
            "    \"phone1\":\"565443\"\n" +
            "}";
    @Mock
    private UserDAO dao;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    private StringWriter stringWriter;
    private PrintWriter writer;

    @BeforeEach
    void setUp() throws IOException {
        stringWriter = new StringWriter();
        writer = new PrintWriter(stringWriter);
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        dao = mock(UserDAO.class);
        when(response.getWriter()).thenReturn(writer);
    }

    @Test
    void doGet_mustReturnUserListByDefaultPageSizeAndPageNumber_whenRequestNotContainsParameter() throws IOException, ServletException {
        final String exceptUserList = "[{\"id\":1,\"userRole\":\"ADMIN\",\"name\":\"user1\",\"surname\":\"fUser1\",\"email\":\"123456@mail.ru\",\"phone\":\"888999\"}," +
                "{\"id\":2,\"userRole\":\"CLIENT\",\"name\":\"user2\",\"surname\":\"fUser2\",\"email\":\"123456@mail.ru\",\"phone\":\"888999\"}," +
                "{\"id\":3,\"userRole\":\"CLIENT\",\"name\":\"user3\",\"surname\":\"fUser3\",\"email\":\"123456@mail.ru\",\"phone\":\"888999\"}," +
                "{\"id\":4,\"userRole\":\"CLIENT\",\"name\":\"user4\",\"surname\":\"fUser4\",\"email\":\"123456@mail.ru\",\"phone\":\"888999\"}," +
                "{\"id\":5,\"userRole\":\"CLIENT\",\"name\":\"user5\",\"surname\":\"fUser5\",\"email\":\"123456@mail.ru\",\"phone\":\"888999\"}]";
        final List<User> userList = new ArrayList<>();
        userList.add(new User(1, UserRole.ADMIN, "user1", "fUser1", "123456@mail.ru", "888999"));
        userList.add(new User(2, UserRole.CLIENT, "user2", "fUser2", "123456@mail.ru", "888999"));
        userList.add(new User(3, UserRole.CLIENT, "user3", "fUser3", "123456@mail.ru", "888999"));
        userList.add(new User(4, UserRole.CLIENT, "user4", "fUser4", "123456@mail.ru", "888999"));
        userList.add(new User(5, UserRole.CLIENT, "user5", "fUser5", "123456@mail.ru", "888999"));
        final HashMap<String, String[]> requestMap = new HashMap<>();

        when(request.getParameterMap()).thenReturn(requestMap);
        when(dao.getAll(anyInt(), anyInt())).thenReturn(userList);

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doGet(request, response);
            verify(response).setStatus(200);
            verify(dao).getAll(1, 5);
            assertThat(stringWriter.toString()).isEqualTo(exceptUserList);
        }
    }

    @Disabled
    @Test
    void doGet_mustReturnUserList_whenParameterContainsPageNumberAndPageSize() throws IOException, ServletException {
        final String pageNumber = "6";
        final String pageSize = "9";
        final HashMap<String, String[]> requestMap = new HashMap<>();
        requestMap.put("page", new String[]{pageNumber});
        requestMap.put("pageSize", new String[]{pageSize});

        when(request.getParameterMap()).thenReturn(requestMap);
        when(request.getParameter("page")).thenReturn(pageNumber);
        when(request.getParameter("pageSize")).thenReturn(pageSize);

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doGet(request, response);
            verify(response).setStatus(200);
            verify(dao).getAll(Integer.parseInt(pageNumber), Integer.parseInt(pageSize));
        }
    }

    @Disabled
    @Test
    void doGet_mustReturnUser_whenRequestContainsIdParameter() throws IOException, ServletException {
        final String exceptUserString = "{\"id\":1,\"userRole\":\"ADMIN\",\"name\":\"user1\",\"surname\":\"fUser1\",\"email\":" +
                "\"123456@mail.ru\",\"phone\":\"888999\"}";
        final String id = "1";
        final HashMap<String, String[]> requestMap = new HashMap<>();
        requestMap.put("id", new String[]{id});
        User userDB = new User(1, UserRole.ADMIN, "user1", "fUser1", "123456@mail.ru", "888999");

        when(request.getParameterMap()).thenReturn(requestMap);
        when(request.getParameter("id")).thenReturn(id);
        when(dao.getById(anyInt())).thenReturn(userDB);

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doGet(request, response);
            verify(dao).getById(Integer.parseInt(id));
            verify(response).setStatus(200);
            assertThat(stringWriter.toString()).isEqualTo(exceptUserString);
        }
    }

    @Test
    void doGet_mustReturnUser_whenRequestContainsEmailParameter() throws IOException, ServletException {
        final String exceptUserString = "{\"id\":1,\"userRole\":\"ADMIN\",\"name\":\"user1\",\"surname\":\"fUser1\",\"email\":" +
                "\"123456@mail.ru\",\"phone\":\"888999\"}";
        final String email = "123456@mail.ru";
        final HashMap<String, String[]> requestMap = new HashMap<>();
        requestMap.put("email", new String[]{email});
        User userDB = new User(1, UserRole.ADMIN, "user1", "fUser1", "123456@mail.ru", "888999");

        when(request.getParameterMap()).thenReturn(requestMap);
        when(request.getParameter("email")).thenReturn(email);
        when(dao.getUserByEmail(anyString())).thenReturn(userDB);

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doGet(request, response);
            verify(dao).getUserByEmail(email);
            verify(response).setStatus(200);
            assertThat(stringWriter.toString()).isEqualTo(exceptUserString);
        }
    }

    @Disabled
    @Test
    void doGet_mustReturnErrorMessage_whenParameterIdNotValid() throws IOException, ServletException {
        final String id = "1asd";
        final HashMap<String, String[]> parameterMap = new HashMap<>();
        parameterMap.put("id", new String[]{id});

        when(request.getParameter("id")).thenReturn(id);
        when(request.getParameterMap()).thenReturn(parameterMap);
        new UserServlet().doGet(request, response);
        verify(response).setStatus(500);
        assertThat(stringWriter.toString()).contains("Ошибка в параметре id = " + id);
    }

    @Test
    void doGet_mustReturnErrorMessage_whenDAOThrowException() throws IOException, ServletException {
        final String id = "1";
        final HashMap<String, String[]> parameterMap = new HashMap<>();
        parameterMap.put("id", new String[]{id});

        when(request.getParameter("id")).thenReturn(id);
        when(request.getParameterMap()).thenReturn(parameterMap);
        when(dao.getById(anyInt())).thenThrow(UserDAOException.class);

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doGet(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Ошибка при получении информации о пользователях");
        }
    }

    @Disabled
    @Test
    void doPost_mustReturnCode204_WhenJsonValid() throws IOException, ServletException {
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(validUserJson)));
        when(dao.getUserByEmail(anyString())).thenReturn(null);
        when(dao.insertRecord(any())).thenReturn(1);

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doPost(request, response);
            verify(response).setStatus(204);
        }
    }

    @Test
    void doPost_mustReturnErrorMessage_WhenJsonNotValid() throws IOException, ServletException {
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(notValidUserJson)));
        when(dao.getUserByEmail(anyString())).thenReturn(null);
        when(dao.insertRecord(any())).thenReturn(1);

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doPost(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Ошибка во входном сообщении. " +
                    "Проверьте правильность переданных данных.");
        }
    }

    @Disabled
    @Test
    void doPost_mustReturnErrorMessage_WhenUserExists() throws IOException, ServletException {
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(validUserJson)));
        when(dao.getUserByEmail(anyString())).thenReturn(new User());

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doPost(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Пользователь с таким email уже существует.");
        }
    }

    @Test
    void doPost_mustReturnErrorMessage_whenDAOThrowException() throws IOException, ServletException {
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(validUserJson)));
        when(dao.getUserByEmail(anyString())).thenReturn(null);
        when(dao.insertRecord(any())).thenThrow(UserDAOException.class);

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doPost(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Ошибка при создании пользователя");
        }
    }

    @Disabled
    @Test
    void doPut_mustReturnCode201_whenIdAndJsonValid() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1");
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(validUserJson)));
        when(dao.getUserByEmail(anyString())).thenReturn(null);
        doNothing().when(dao).updateRecord(anyInt(), any());

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doPut(request, response);
            verify(response).setStatus(201);
        }
    }

    @Test
    void doPut_mustErrorMessage_whenIdNotValid() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1asd");
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(validUserJson)));
        when(dao.getUserByEmail(anyString())).thenReturn(null);
        doNothing().when(dao).updateRecord(anyInt(), any());

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doPut(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Ошибка в параметре id");
        }
    }

    @Disabled
    @Test
    void doPut_mustErrorMessage_whenEmailExists() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1");
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(validUserJson)));
        when(dao.getUserByEmail(anyString())).thenReturn(new User());
        doNothing().when(dao).updateRecord(anyInt(), any());

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doPut(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Пользователь с таким email уже существует.");
        }
    }

    @Test
    void doPut_mustErrorMessage_whenJsonNotValid() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1");
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(notValidUserJson)));
        when(dao.getUserByEmail(anyString())).thenReturn(null);
        doNothing().when(dao).updateRecord(anyInt(), any());

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doPut(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Ошибка во входном сообщении. " +
                    "Проверьте правильность названия переданных полей.");
        }
    }

    @Test
    void doPut_mustErrorMessage_whenDAOThrowException() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1");
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(validUserJson)));
        when(dao.getUserByEmail(anyString())).thenReturn(null);
        doThrow(UserDAOException.class).when(dao).updateRecord(anyInt(), any());

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doPut(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Ошибка при обновлении данных о пользователе");
        }
    }

    @Test
    void doDelete_mustReturnCode204_whenParameterIdValid() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1");
        doNothing().when(dao).deleteRecord(anyInt());

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doDelete(request, response);
            verify(response).setStatus(204);
            assertThat(stringWriter.toString()).isEmpty();
        }
    }

    @Test
    void doDelete_mustReturnErrorMessage_whenParameterIdNotValid() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1asd");
        doNothing().when(dao).deleteRecord(anyInt());

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doDelete(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Ошибка в параметре id");
        }
    }

    @Test
    void doDelete_mustReturnErrorMessage_whenDAOThrowException() throws IOException, ServletException {
        when(request.getParameter("id")).thenReturn("1");
        doThrow(UserDAOException.class).when(dao).deleteRecord(anyInt());

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new UserServlet().doDelete(request, response);
            verify(response).setStatus(500);
            assertThat(stringWriter.toString()).contains("Не удалось удалить пользователя c id = 1");
        }
    }
}