package com.tsc.ProductWarehouse.domain.enums;

/**
 * Роли пользователя
 *
 * @author Дмитрий Лаврухин
 */
public enum UserRole {
    CLIENT,
    ADMIN
}
