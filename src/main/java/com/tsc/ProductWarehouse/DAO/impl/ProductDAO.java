package com.tsc.ProductWarehouse.DAO.impl;

import com.tsc.ProductWarehouse.DAO.exceptions.ProductDAOException;
import com.tsc.ProductWarehouse.DAO.mappers.ProductListMapper;
import com.tsc.ProductWarehouse.DAO.mappers.ProductMapper;
import com.tsc.ProductWarehouse.DAO.queryStringBuilder.ProductQueryStringBuilder;
import com.tsc.ProductWarehouse.domain.entity.Product;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static java.util.Objects.isNull;

/**
 * DAO объект с методами для управления продуктами
 *
 * @author Дмитрий Лаврухин
 */
public class ProductDAO extends DefaultDao<Product> {

    private static ProductDAO instance;

    private ProductDAO() {
        super(new ProductMapper(), new ProductListMapper());
    }

    public static ProductDAO getInstance() {
        if (isNull(instance)) {
            instance = new ProductDAO();
        }
        return instance;
    }

    @Override
    public List<Product> getAll(final int pageNumber, final int pageSize) {
        final String queryString = ProductQueryStringBuilder.builder()
                .queryAllProduct()
                .pageNumber(pageNumber, pageSize)
                .build();
        return super.getAll(queryString);
    }

    @Override
    public Product getById(final int id) {
        final String queryString = ProductQueryStringBuilder.builder()
                .queryAllProduct()
                .where()
                .id()
                .build();
        return super.getById(queryString, id);
    }

    @Override
    public void deleteRecord(final int id) {
        final String queryString = ProductQueryStringBuilder.builder()
                .delete()
                .where()
                .id()
                .build();
        super.deleteRecord(queryString, id);
    }

    @Override
    public int insertRecord(final Product newRecord) {
        final String queryString = ProductQueryStringBuilder.builder()
                .insert()
                .build();
        try (final Connection connection = DriverManager.getConnection(urlConnectDB, dbUser, dbPassword);
             final PreparedStatement statement = connection.prepareStatement(queryString, Statement.RETURN_GENERATED_KEYS)) {
            fillStatement(newRecord, statement);
            final int newId = statement.executeUpdate();
            if (newId == 0) {
                throw new SQLException("Не удалось добавить запись в базу данных.");
            }
            return newId;
        } catch (SQLException e) {
            throw new ProductDAOException(e);
        }
    }

    @Override
    public void updateRecord(final int productId, final Product newRecord) {
        final String queryString = ProductQueryStringBuilder.builder()
                .update()
                .where()
                .id()
                .build();
        try (final Connection connection = DriverManager.getConnection(urlConnectDB, dbUser, dbPassword);
             final PreparedStatement statement = connection.prepareStatement(queryString)) {
            fillStatement(newRecord, statement);
            statement.setInt(6, productId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new ProductDAOException(e);
        }
    }

    private void fillStatement(Product newRecord, PreparedStatement statement) throws SQLException {
        statement.setString(1, newRecord.getName());
        statement.setString(2, newRecord.getProductType().name());
        statement.setDouble(3, newRecord.getPrice());
        statement.setDate(4, Date.valueOf(newRecord.getCreationDate()));
        statement.setDate(5, Date.valueOf(newRecord.getExpirationDate()));
    }

    public List<Product> getProductByProductType(final String type) {
        final String queryString = ProductQueryStringBuilder.builder()
                .queryAllProduct()
                .where()
                .type()
                .build();
        return getProductWithParameterParameter(queryString, type);
    }

    public List<Product> getProductByProductName(final String name) {
        final String queryString = ProductQueryStringBuilder.builder()
                .queryAllProduct()
                .where()
                .name()
                .build();
        return getProductWithParameterParameter(queryString, name);
    }

    public List<Product> getFreshProducts() {
        final String queryString = ProductQueryStringBuilder.builder()
                .queryAllProduct()
                .onlyFresh()
                .build();
        try (final Connection connection = DriverManager.getConnection(urlConnectDB, dbUser, dbPassword);
             final PreparedStatement statement = connection.prepareStatement(queryString)) {
            statement.setDate(1, new Date(System.currentTimeMillis()));
            try (final ResultSet rs = statement.executeQuery()) {
                return listMapper.mapRow(rs);
            }
        } catch (SQLException e) {
            throw new ProductDAOException(e);
        }
    }

    private List<Product> getProductWithParameterParameter(final String queryString, final String parameter) {
        try (final Connection connection = DriverManager.getConnection(urlConnectDB, dbUser, dbPassword);
             final PreparedStatement statement = connection.prepareStatement(queryString)) {
            statement.setString(1, parameter);
            try (final ResultSet rs = statement.executeQuery()) {
                return listMapper.mapRow(rs);
            }
        } catch (SQLException e) {
            throw new ProductDAOException(e);
        }
    }
}
