package com.tsc.ProductWarehouse.domain.enums;

/**
 * Тип продукта
 *
 * @author Дмитрий Лаврухин
 */
public enum ProductType {
    BREAD,
    MILK,
    CHEESE
}
