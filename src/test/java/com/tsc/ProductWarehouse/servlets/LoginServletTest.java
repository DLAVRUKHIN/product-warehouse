package com.tsc.ProductWarehouse.servlets;

import com.tsc.ProductWarehouse.DAO.impl.UserDAO;
import com.tsc.ProductWarehouse.domain.entity.User;
import com.tsc.ProductWarehouse.domain.enums.UserRole;
import com.tsc.ProductWarehouse.util.JWTUtil;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class LoginServletTest {

    private final String userAdminJwt = "Bearer eyJ0eXBlIjoiSldUIiwiYWxnIjoiSFM1MTIifQ.eyJzdWIiOiIxMiIsImVtYWlsIjoi" +
            "MTIzQG1haWwucnUiLCJyb2wiOiJBRE1JTiJ9.1Db3KHqPOEyBkZSfE6SEvwUhuZ452qz1fV_rMoVrGPu1zV0ZbY7FwcKgzfBQhHm_F6" +
            "nfyTS-DiFOtyrBOVi9Ew";

    private final String loginAndPassword = "{\n" +
            "    \"email\" : \"123@mail.ru\",\n" +
            "    \"password\" : \"123456\"\n" +
            "}";

    private final String emptyLoginAndPassword = "{\n" +
            "    \"email\" : \"\",\n" +
            "    \"password\" : \"123456\"\n" +
            "}";

    @Mock
    private UserDAO dao;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;

    private StringWriter stringWriter;
    private PrintWriter writer;

    @BeforeEach
    void setUp() throws IOException {
        stringWriter = new StringWriter();
        writer = new PrintWriter(stringWriter);

        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        dao = mock(UserDAO.class);

        when(response.getWriter()).thenReturn(writer);
    }

    @Test
    void doPost_shouldReturnJWT_WhenUserExists() throws IOException, ServletException {
        final User user = new User(12, UserRole.ADMIN, "test", "testoviy", "123@mail.ru", "123321");
        user.setPassword("password");

        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(loginAndPassword)));
        when(dao.getUserByEmailAndPassword(anyString(), anyString())).thenReturn(user);

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new LoginServlet().doPost(request, response);
            verify(response).addHeader(JWTUtil.TOKEN_HEADER, userAdminJwt);
        }
    }

    @Disabled
    @Test
    void doPost_shouldReturnErrorMessage_WhenUserNotExist() throws IOException, ServletException {
        when(dao.getUserByEmailAndPassword(anyString(), anyString())).thenReturn(null);
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(loginAndPassword)));

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new LoginServlet().doPost(request, response);
            verify(response).setStatus(401);
            assertThat(stringWriter.toString()).contains("Пользователь с таким логином/паролем не найден");
        }
    }

    @Disabled
    @Test
    void doPost_shouldReturnErrorMessage_WhenLoginIsEmpty() throws IOException, ServletException {
        when(request.getReader()).thenReturn(new BufferedReader(new StringReader(emptyLoginAndPassword)));

        try (final MockedStatic<UserDAO> theMock = Mockito.mockStatic(UserDAO.class)) {
            theMock.when(UserDAO::getInstance).thenReturn(dao);
            new LoginServlet().doPost(request, response);
            verify(response).setStatus(401);
            assertThat(stringWriter.toString()).contains("Не указан логин/пароль");
        }
    }
}